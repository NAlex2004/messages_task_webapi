import * as $ from 'jquery'

export async function getMessages(queryObject) {
    let result = await $.ajax({
        url : "/api/messages",
        method : "GET",
        data: queryObject
        // data: { UserName: "", OrderByField: "User.UserName", OrderByDesc: true}
    });

    return result;
}

export async function sendMessage(userName, messageText) {
    // let formData = new FormData();
    // formData.append("UserName", userName);
    // formData.append("MessageText", messageText);
    let result = await $.ajax({
        url : "/api/messages",
        method : "POST",
        data: { UserName: userName, MessageText: messageText }
    });

    return result; 
}
