import React, { Component } from 'react';
// import logo from '../logo.svg';
// import '../App.css';
import MessageList from './MessageList';
import * as Service from '../service'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Nav from './Nav'
import Main from './Main';
import Messages from './Messages';
import About from './About';
import Contact from './Contact';

class App extends Component {
  render() {
    return (
      <div className="container body-content">        
        <Router>                      
          <div>
            <Nav/>
            <br />
              <Switch>
                <Route exact path="/" component={Main} />
                <Route path="/Messages" component={Messages} />
                <Route path="/About" component={About} />
                <Route path="/Contact" component={Contact} />
              </Switch>
          </div>
        </Router>        
      </div>
    );
  }
}

export default App;
