import React from 'react';
import {Link} from 'react-router-dom';

class Nav extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-inverse navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <Link className="navbar-brand" to="/">React App</Link>
                    </div>
                    <div className="navbar-collapse collapse">
                        <ul className="nav navbar-nav">
                                    <li><Link to='/'>Home</Link></li>
                                    <li><Link to='/Messages'>Messages</Link></li>
                                    <li><Link to='/About'>About</Link></li>
                                    <li><Link to='/Contact'>Contact</Link></li>
                                    {/* <li><a href="/">Home</a></li>                                    
                                    <li><a href="/Messages">Messages</a></li>
                                    <li><a href="/About">About</a></li>
                                    <li><a href="/Contact">Contact</a></li> */}
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Nav;