import React from 'react';
import * as Service from '../service';

class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
                        UserName: "",
                        OrderByField: "MessageDate",
                        OrderByDesc: true                       
                    };

        this.onUserNameChange = this.onUserNameChange.bind(this);
        this.onOrderByChange = this.onOrderByChange.bind(this);
        this.onOrderByDescChange = this.onOrderByDescChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onUserNameChange(event) {
        let val = event.target.value;
        this.setState({ UserName : val });
    }

    onOrderByChange(event) {
        let val = event.target.value;
        this.setState({ OrderByField : val });
    }

    onOrderByDescChange(event) {
        let val = event.target.checked;
        this.setState({ OrderByDesc : val });
    }

    handleSubmit(event) {       
        event.preventDefault(); 
        if (this.props.onFilter) {
            this.props.onFilter(this.state);
        }
    }

    render() {
        return (
            <div>      
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="userName">Filter by User Name </label>
                    <input type="text" name="userName" id="userName" value={this.state.UserName} onChange={this.onUserNameChange} />                                                        
                    <label htmlFor="orderByFieldName">Sort By </label>
                    <select name="orderByField" id="orderByField" onChange={this.onOrderByChange} value = {this.state.OrderByField}>
                        <option value="MessageDate">Date</option>
                        <option value="User.Guid">User Id</option>
                        <option value="User.UserName">User Name</option>
                        <option value="MessageText">Message Text</option>
                    </select>
                    <label htmlFor="desc">Sort descending </label>
                    <input type="checkbox" name="desc" id="desc" defaultChecked={this.state.OrderByDesc} onClick={this.onOrderByDescChange} />
                    {/* <input type="hidden" name="desc" value="false" /> */}
                    <button className="btn btn-sm btn-primary" type="submit" style={{marginLeft: "35px", width: "80px"}} >OK</button>    
                </form>    
            </div>
        );
    }
}

export default Filter;