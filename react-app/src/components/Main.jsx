import React from 'react';
import MessageList from './MessageList';
import MessageForm from './MessageForm';
import * as Service from '../service';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = { messages: [] };
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.getLatestMessages = this.getLatestMessages.bind(this);
    }

    getLatestMessages() {
        Service.getMessages({ Page: 1, OrderByField: "MessageDate", OrderByDesc: true }).then(response => {
            this.setState( { messages: response } );
        });
    }

    componentDidMount() {
        this.getLatestMessages();
        this.timerId = setInterval(this.getLatestMessages, 10000);
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    handleFormSubmit(message) {
        if (message) {
            Service.sendMessage(message.Name, message.Message).then(response => {
                if (!response.hasError) {
                    this.getLatestMessages();
                    return;
                }

                // error handling..
                alert("Error: " + response.errorMessage);
            });
        }
    }

    render() {
        return (
            <div>
                <MessageForm onSubmit={this.handleFormSubmit} />
                <hr/>
                <div>
                    <label className="label-head">Last messages:</label>                    
                </div>
                <hr style={{margin: "0"}}/>    
                <MessageList messages={this.state.messages} />         
            </div>                        
        );
    }
}

export default Main;