import React, { Component } from 'react';


class MessageForm extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = { Name: "", Message: ""};        
        this.state.handleSubmit = props.onSubmit;

        this.submit = this.submit.bind(this);
        this.nameChanged = this.nameChanged.bind(this);
        this.messageChanged = this.messageChanged.bind(this);
    }
    
    submit(event) {
        event.preventDefault();
        if (this.state.handleSubmit){
            this.state.handleSubmit({ Name: this.state.Name, Message: this.state.Message });
        }
    }

    nameChanged(event) {
        var value = event.target.value;
        this.setState({Name: value});
    }

    messageChanged(event) {
        var value = event.target.value;
        this.setState({Message: value});
    }

    render() {
        return (
            <div className="container">
                <form onSubmit={this.submit}>
                    <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input className="form-control" id="name" value = {this.state.Name} placeholder="Your name" onChange={this.nameChanged}></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="message">Message</label>
                        <textarea className="form-control" id="message" value = {this.state.Message} placeholder="Message text" rows="5" onChange={this.messageChanged}></textarea>
                    </div>
                    <button type="submit" value="submit" className="btn btn-info">Send</button>
                </form>
            </div>
        );
    }
}

export default MessageForm;