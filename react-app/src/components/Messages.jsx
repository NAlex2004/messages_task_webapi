import React from 'react';
import MessageList from './MessageList';
import Filter from './Filter';
import * as Service from '../service';

class Messages extends React.Component {
    constructor(props) {
        super(props);
        this.state = { messages: [] };                
        this.handleFilter = this.handleFilter.bind(this);        
    }

    componentDidMount() {
        Service.getMessages({OrderByField: "MessageDate", OrderByDesc: true }).then(response => {
            // console.log(resonce);
            this.setState( { messages: response } );
        });
    }

    handleFilter(filterQuery) {   
        Service.getMessages(filterQuery)
            .then(response => {
                this.setState({messages: response});
            });
    }

    render() {
        return (
            <div className="container-fluid form-group">
                <div>
                    <h3 className="h3 label-head">Messages:</h3>                    
                </div>
                <hr/>
                <Filter onFilter={this.handleFilter} />
                <hr/>
                <MessageList messages={this.state.messages} />                
            </div>
        );
    }
}

export default Messages;