import React from 'react';

class MessageList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (Array.isArray(this.props.messages))
        {
            return (
                <table className="table-striped">
                    <thead>
                        <tr>
                            <th style={{width: "15%"}}>
                                Date
                            </th>
                            <th style={{width: "15%"}}>
                                User Id
                            </th>
                            <th style={{width: "20%"}}>
                                User Name
                            </th>
                            <th>
                                Message Text
                            </th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign: "left"}}>
                        {
                            this.props.messages.map(function(item) {
                                let date = new Date(item.messageDate);
                                let localeDate = date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
                                return (
                                    <tr key={item.guid}>
                                        <td>
                                            {localeDate}
                                        </td>
                                        <td>
                                            {item.user.guid}
                                        </td>
                                        <td>
                                            {item.user.userName}
                                        </td>
                                        <td>
                                            {item.messageText}    
                                        </td>
                                    </tr>                                    
                                );
                            })
                        }
                    </tbody>
                </table>
            );
        }

        return (
            <div></div>
        );
    }
}

export default MessageList;