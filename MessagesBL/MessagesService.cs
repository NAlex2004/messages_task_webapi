﻿using System;
using System.Collections.Generic;
using System.Text;
using Messages.DAL;
using Messages.DAL.Items;
using NAlex.SimpleMapper;
using NAlex.SimpleMapper.Expressions;
using Messages.BL.DTO;
using System.Linq;
using System.Linq.Expressions;
using Messages.BL.Units;
using Messages.BL.Extensions;
using Microsoft.Extensions.Configuration;

namespace Messages.BL
{
    public class MessagesService: IDisposable
    {
        class UserMessageItem
        {
            public UserItem User { get; set; }
            public MessageItem Message { get; set; }
        }

        IMessagesUnitOfWork _unitOfWork;
        SimpleMapper _mapper = new SimpleMapper();
        int _maxMessagesOnPage = 0;

        public MessagesService(IMessagesUnitOfWork unitOfWork, IConfiguration configuration = null)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException();
            if (configuration != null)
            {
                int.TryParse(configuration["MaxMessagesOnPage"], out _maxMessagesOnPage);
            }

            _mapper.CreateMap<User, UserItem>();
            _mapper.CreateMap<UserItem, User>();
            _mapper.CreateMap<Message, MessageItem>();
            _mapper.CreateMap<MessageItem, Message>();
        }

        private UserItem GetOrAddUser(string userName)
        {
            var userItem = _unitOfWork.Users.Get(u => u.UserName == userName).FirstOrDefault();

            if (userItem == null)
            {
                userItem = new UserItem()
                {
                    UserName = userName
                };
                userItem = _unitOfWork.Users.Add(userItem);
                _unitOfWork.SaveChanges();                
            }

            return userItem;
        }

        public Message SendMessage(string userName, string messageText)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(messageText))
            {
                throw new ArgumentException("userName and messageText cannot be empty.");
            }

            var user = GetOrAddUser(userName);
            MessageItem message = new MessageItem()
            {
                UserGuid = user.Guid,
                MessageText = messageText
            };

            var addedMessageItem = _unitOfWork.Messages.Add(message);
            _unitOfWork.SaveChanges();

            var result = _mapper.Map<MessageItem, Message>(addedMessageItem);
            result.User = _mapper.Map<UserItem, User>(user);

            return result;
        }

        private IOrderedQueryable<UserMessageItem> OrderBy(IQueryable<UserMessageItem> messages, string orderByField, bool orderByDesc)
        {
            if (!string.IsNullOrEmpty(orderByField))
            {
                // если не по полям User
                if (!orderByField.Contains('.'))
                {
                    orderByField = $"Message.{orderByField}";
                }

                return messages.OrderBy(orderByField, orderByDesc);
            }

            return messages.OrderBy("Message.MessageDate", orderByDesc);            
        }

        public IEnumerable<Message> GetMessages(MessageQueryModel query = null)
        {
            query = query ?? new MessageQueryModel();

            var messageItems = _unitOfWork.Users.Get()
                                .Join(_unitOfWork.Messages.Get(), u => u.Guid, m => m.UserGuid, (u, m) => new UserMessageItem{ User = u, Message = m });
            if (!string.IsNullOrEmpty(query.UserName))
            {
                messageItems = messageItems.Where(g => g.User.UserName.Contains(query.UserName, StringComparison.OrdinalIgnoreCase));
            }

            int page = query.Page;
            var loadedMessages = OrderBy(messageItems, query.OrderByField, query.OrderByDesc)
                                .GetPage(ref page, _maxMessagesOnPage)
                                .ToArray();
            
            var result = loadedMessages.Select(m =>
                                {
                                    var message = _mapper.Map<MessageItem, Message>(m.Message);
                                    message.User = _mapper.Map<UserItem, User>(m.User);
                                    return message;
                                });

            return result;
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _unitOfWork?.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {            
            Dispose(true);         
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
