﻿using System;
using System.Collections.Generic;
using System.Text;
using Messages.DAL;
using Messages.DAL.Items;
using Microsoft.Extensions.Configuration;

namespace Messages.BL.Units
{
    public class MessagesInMemoryUnitOfWork : IMessagesUnitOfWork
    {
        public IRepository<MessageItem> Messages { get; protected set; }
        public IRepository<UserItem> Users { get; protected set; }

        public int SaveChanges()
        {
            return 1;
        }

        public MessagesInMemoryUnitOfWork(IConfiguration configuration)
        {
            int maxMessageCountPerUser;
            int maxMessageCount;
            int.TryParse(configuration["MaxMessageCount"], out maxMessageCount);
            int.TryParse(configuration["MaxMessageCountPerUser"], out maxMessageCountPerUser);
            Messages = new MessageInMemoryRepository(maxMessageCount, maxMessageCountPerUser);
            Users = new UserInMemoryRepository();
        }

        #region IDisposable Support
        public void Dispose()
        {
        }
        #endregion
    }
}
