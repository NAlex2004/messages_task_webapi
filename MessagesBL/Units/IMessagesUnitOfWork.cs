﻿using System;
using System.Collections.Generic;
using System.Text;
using Messages.DAL;
using Messages.DAL.Items;

namespace Messages.BL.Units
{
    public interface IMessagesUnitOfWork: IDisposable
    {
        IRepository<MessageItem> Messages { get; }
        IRepository<UserItem> Users { get; }

        int SaveChanges();
    }
}
