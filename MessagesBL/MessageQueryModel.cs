﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Messages.BL
{
    public class MessageQueryModel
    {
        public string UserName { get; set; }
        public string OrderByField { get; set; }
        public bool OrderByDesc { get; set; }
        public int Page { get; set; }
    }
}
