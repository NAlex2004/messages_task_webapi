﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace Messages.BL.Extensions
{
    public static class Extensions
    {
        public static IQueryable<T> GetPage<T>(this IQueryable<T> source, ref int page, int itemsPerPage)
        {
            if (page > 0 && itemsPerPage > 0)
            {
                int messageCount = source.Count();

                int totalPages = messageCount / itemsPerPage + (messageCount % itemsPerPage > 0 ? 1 : 0);
                if (page > totalPages)
                {
                    page = totalPages;
                }
                int skip = (page - 1) * itemsPerPage;

                return source.Skip(skip).Take(itemsPerPage);
            }

            return source;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property, bool desc = false)
        {            
            if (string.IsNullOrEmpty(property))
            {
                return source.OrderBy(i => i);
            }
            Type type = typeof(T);
            ParameterExpression parameter = Expression.Parameter(type, "x");

            Expression expr = parameter;
            string methodName = desc ? "OrderByDescending" : "OrderBy";

            string[] props = property.Split('.');

            foreach (string prop in props)
            {
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.MakeMemberAccess(expr, pi);
                type = pi.PropertyType;                
            }
            
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, parameter);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), type)
                    .Invoke(null, new object[] { source, lambda });
            return (IOrderedQueryable<T>)result;
        }

        public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> source, string property, bool desc = false)
        {
            return source.AsQueryable().OrderBy(property, desc).AsEnumerable().OrderBy(i => 1);
        }
    }
}
