﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages.BL.DTO
{
    public class Message
    {
        public Guid Guid { get; set; }
        public User User { get; set; }
        public string MessageText { get; set; }
        public DateTime MessageDate { get; set; }
    }
}
