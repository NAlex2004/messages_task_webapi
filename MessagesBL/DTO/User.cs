﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages.BL.DTO
{
    public class User
    {
        public Guid Guid { get; set; }
        public string UserName { get; set; }
    }
}
