﻿using Messages.DAL.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Messages.DAL
{
    public class UserInMemoryRepository : GenericInMemoryRepository<UserItem>
    {
        public override UserItem Add(UserItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            if (string.IsNullOrEmpty(item.UserName))
            {
                throw new ArgumentException("UserName cannot be empty.");
            }

            return base.Add(item);
        }
    }
}
