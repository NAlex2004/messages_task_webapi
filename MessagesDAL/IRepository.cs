﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Messages.DAL
{
    public interface IRepository<TItem>
    {
        TItem Add(TItem item);
        bool Remove(TItem item);
        int Remove(Expression<Func<TItem, bool>> expression);
        IQueryable<TItem> Get(Expression<Func<TItem, bool>> expression = null);
    }
}
