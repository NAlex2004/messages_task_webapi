﻿using Messages.DAL.Items;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Messages.DAL
{
    public class MessageInMemoryRepository: GenericInMemoryRepository<MessageItem>
    {
        int _maxMessagesCount;
        int _maxMessagesPerUserCount;

        protected bool UserMessagesLimitReached(Guid userGuid)
        {
            if (_maxMessagesPerUserCount <= 0)
            {
                return false;
            }

            int userMessagesCount = _items.Count(m => m.UserGuid == userGuid);

            return userMessagesCount >= _maxMessagesPerUserCount;
        }

        protected bool MessagesLimitReached()
        {
            return _maxMessagesCount > 0
                ? _items.Count >= _maxMessagesCount
                : false;
        }

        public override MessageItem Add(MessageItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            if (item.UserGuid == Guid.Empty)
            {
                throw new ArgumentException("UserGuid of message cannot be empty.");
            }

            if (string.IsNullOrEmpty(item.MessageText))
            {
                throw new ArgumentException("MessageText cannot be empty.");
            }
            
            if (item.MessageDate == DateTime.MinValue)
            {
                item.MessageDate = DateTime.UtcNow;
            }

            IEnumerable<MessageItem> items = null;
            if (UserMessagesLimitReached(item.UserGuid))
            {
                items = _items.Where(m => m.UserGuid == item.UserGuid);
            }
            else if (MessagesLimitReached())
            {
                items = _items;
            }

            if (items != null)
            {
                var itemToRemove = items.OrderBy(m => m.MessageDate).FirstOrDefault();
                Remove(itemToRemove);
            }

            return base.Add(item);
        }

        /// <summary>
        /// Stores Messages in List, maxMessagesCount or maxMessagesPerUserCount = 0 - unlimited count of messages, or messages per user
        /// </summary>
        /// <param name="maxMessagesCount">0 - unlimited</param>
        /// <param name="maxMessagesPerUserCount">0 - unlimited</param>
        public MessageInMemoryRepository(int maxMessagesCount = 0, int maxMessagesPerUserCount = 0)
        {
            _maxMessagesCount = maxMessagesCount;
            _maxMessagesPerUserCount = maxMessagesPerUserCount;
        }
    }
}
