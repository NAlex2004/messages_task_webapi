﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages.DAL.Items
{
    public class UserItem: IEquatable<UserItem>
    {
        public Guid Guid { get; protected set; }
        public string UserName { get; set; }

        public UserItem()
        {
            Guid = Guid.NewGuid();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as UserItem);
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode() ^ UserName.GetHashCode();
        }

        public bool Equals(UserItem other)
        {
            if (other == null)
            {
                return false;
            }

            return Guid == other.Guid && UserName == other.UserName;
        }
    }
}
