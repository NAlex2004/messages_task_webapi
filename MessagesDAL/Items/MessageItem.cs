﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages.DAL.Items
{
    public class MessageItem: IEquatable<MessageItem>
    {
        public Guid Guid { get; protected set; }
        public Guid UserGuid { get; set; }
        public string MessageText { get; set; }
        public DateTime MessageDate { get; set; }

        public MessageItem()
        {
            Guid = Guid.NewGuid();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as MessageItem);
        }

        public override int GetHashCode()
        {
            return Guid.GetHashCode() ^ UserGuid.GetHashCode() ^ MessageText.GetHashCode() ^ MessageDate.GetHashCode();
        }

        public bool Equals(MessageItem other)
        {
            if (other == null)
            {
                return false;
            }

            return Guid == other.Guid && UserGuid == other.UserGuid && MessageText == other.MessageText && MessageDate == other.MessageDate;
        }
    }
}
