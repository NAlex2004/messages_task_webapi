﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Messages.DAL
{
    public abstract class GenericInMemoryRepository<TItem> : IRepository<TItem>
    {
        protected List<TItem> _items = new List<TItem>();

        public virtual TItem Add(TItem item)
        {
            _items.Add(item);

            return item;
        }

        public IQueryable<TItem> Get(Expression<Func<TItem, bool>> expression = null)
        {
            var items = expression != null
                ? _items.AsQueryable().Where(expression)
                : _items.AsQueryable();

            return items;
        }

        public bool Remove(TItem item)
        {
            return _items.Remove(item);
        }

        public int Remove(Expression<Func<TItem, bool>> expression)
        {
            if (expression == null)
            {
                return 0;
            }
            Predicate<TItem> predicate = new Predicate<TItem>(expression.Compile());
            int removedCount = _items.RemoveAll(predicate);

            return removedCount;

            //var itemsToRemove = Get(expression).ToArray();
            //int removedCount = 0;

            //foreach(var item in itemsToRemove)
            //{
            //    removedCount += Remove(item) ? 1 : 0;
            //}

            //return removedCount;
        }

    }
}
