﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages.DAL
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string message) : base(message)
        {
        }
    }
}
