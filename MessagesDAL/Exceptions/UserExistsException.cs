﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages.DAL
{
    public class UserExistsException : Exception
    {
        public UserExistsException(string message) : base(message)
        {
        }
    }
}
