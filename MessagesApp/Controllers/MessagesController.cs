﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Messages.BL;
using Messages.BL.DTO;
using Messages.Application.Models;

namespace Messages.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase, IDisposable
    {
        private static MessagesService _service;        

        private void InitMessages()
        {
            _service.SendMessage("John", "Barr");
            _service.SendMessage("Jack", "Daniel's");
            _service.SendMessage("Uasia", "Pupkin");
            _service.SendMessage("John", "FUCK YOU!!");
            _service.SendMessage("Jack", "Shit happens");
            _service.SendMessage("Uasia", "Ne pizdite!");
            _service.SendMessage("John", "Barrel of beer!");
            _service.SendMessage("Jack", "Daniel's ass");
            _service.SendMessage("Uasia", "Pupkin blyadun");
        }

        public MessagesController(MessagesService service)
        {
            if (_service == null)
            {
                _service = service;
                InitMessages();
            }            
        }

        // GET: api/Messages/john
        //[HttpGet("{userName?}", Name = "Get")]        
        [HttpGet]
        public IEnumerable<Message> Get([FromQuery] MessageQueryModel query)
        {
            var messages = _service.GetMessages(query);
            return messages;
        }

        // POST: api/Messages
        [HttpPost]
        public MessageResponseModel Post([FromForm] MessagePostModel message)
        {
            try
            {
                var postedMessage = _service.SendMessage(message.UserName, message.MessageText);

                return new MessageResponseModel()
                            {
                                Message = postedMessage,
                                HasError = false,
                                ErrorMessage = string.Empty
                            };
            }
            catch (ArgumentException e)
            {
                return new MessageResponseModel()
                            {
                                Message = null,
                                HasError = true,
                                ErrorMessage = e.Message
                            };
            }
        }

        //// PUT: api/Messages/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _service?.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
