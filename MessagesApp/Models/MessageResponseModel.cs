﻿using Messages.BL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messages.Application.Models
{
    public class MessageResponseModel
    {
        public Message Message { get; set; }
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }
    }
}
