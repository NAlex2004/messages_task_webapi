﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Messages.Application.Models
{
    public class MessagePostModel
    {
        public string UserName { get; set; }
        public string MessageText { get; set; }
    }
}
