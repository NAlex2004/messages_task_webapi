﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Messages.DAL;
using Messages.DAL.Items;
using Messages.BL;
using Messages.BL.DTO;
using System.Linq;
using Messages.BL.Units;
using Microsoft.Extensions.Configuration;

namespace UnitTestProject
{
    [TestClass]
    public class BLTests
    {        

        [TestClass]
        public class MessageServiceTests
        {
            MessagesService service; 
            IConfiguration configuration;

            public MessageServiceTests()
            {
                configuration = new Configuration();
                service = new MessagesService(new MessagesInMemoryUnitOfWork(configuration), configuration);
            }

            [TestMethod]
            public void SendMessage_NewUserCreated_ExistingFound()
            {
                service.SendMessage("user", "message");
                service.SendMessage("another", "shit");
                service.SendMessage("user", "text");

                MessageQueryModel query1 = new MessageQueryModel()
                {
                    UserName = "user"
                };
                MessageQueryModel query2 = new MessageQueryModel()
                {
                    UserName = "aaaa"
                };
                int totalMessages = service.GetMessages().Count();
                int userMessages = service.GetMessages(query1).Count();
                int empty = service.GetMessages(query2).Count();

                Assert.AreEqual(3, totalMessages);
                Assert.AreEqual(2, userMessages);
                Assert.AreEqual(0, empty);
            }

            [TestMethod]
            public void SendMessage_Then_Get_MessageHasUser()
            {
                service.SendMessage("user", "message");
                service.SendMessage("another", "shit");
                service.SendMessage("user", "text");

                MessageQueryModel query = new MessageQueryModel()
                {
                    UserName = "user"
                };
                var message = service.GetMessages(query).Where(m => m.MessageText == "text").FirstOrDefault();

                Assert.IsNotNull(message.User);
                Assert.AreEqual("user", message.User.UserName);
                Assert.AreNotEqual(Guid.Empty, message.User.Guid);
            }

            [TestMethod]
            public void MaxMessages_CountCorrect()
            {
                MessageQueryModel vasiaQuery = new MessageQueryModel()
                {
                    UserName = "Vasia"
                };
                MessageQueryModel jackQuery = new MessageQueryModel()
                {
                    UserName = "Jack"
                };
                MessageQueryModel assQuery = new MessageQueryModel()
                {
                    UserName = "Ass"
                };

                for (int i = 0; i < 10; i++)
                {
                    service.SendMessage(vasiaQuery.UserName, $"Vasia Message #{i}");
                }

                int vasiaCount = service.GetMessages(vasiaQuery).Count();
                Assert.AreEqual(int.Parse(configuration["MaxMessageCountPerUser"]), vasiaCount);

                for (int i = 0; i < 10; i++)
                {
                    service.SendMessage(jackQuery.UserName, $"Jack Message #{i}");
                }

                int count = service.GetMessages().Count();
                Assert.AreEqual(int.Parse(configuration["MaxMessageCountPerUser"]) * 2, count);

                for (int i = 0; i < 10; i++)
                {
                    service.SendMessage(assQuery.UserName, $"Ass Message #{i}");
                }

                count = service.GetMessages().Count();
                Assert.AreEqual(int.Parse(configuration["MaxMessageCount"]), count);
            }

            [TestMethod]
            public void MessageService_SortTest()
            {
                service.SendMessage("user", "x-message");
                service.SendMessage("another", "shit");
                service.SendMessage("z-user", "text");

                MessageQueryModel query = new MessageQueryModel()
                {
                    OrderByField = "User.UserName"
                };

                var messages = service.GetMessages(query);
                var first = messages.First();
                Assert.AreEqual("another", first.User.UserName);

                query.OrderByField = "MessageText";
                query.OrderByDesc = true;
                messages = service.GetMessages(query);
                first = messages.First();
                Assert.AreEqual("x-message", first.MessageText);
            }

            [TestMethod]
            public void MessageService_Pages()
            {
                for (int i = 0; i < 20; i++)
                {
                    service.SendMessage($"User_{i % 5 }", $"Message {i}");
                }

                MessageQueryModel query = new MessageQueryModel()
                {
                    Page = 100000
                };

                int maxPages = int.Parse(configuration["MaxMessageCount"]) / int.Parse(configuration["MaxMessagesOnPage"]) +
                    (int.Parse(configuration["MaxMessageCount"]) % int.Parse(configuration["MaxMessagesOnPage"]) > 0 ? 1 : 0);

                var messages = service.GetMessages(query);
                Assert.AreEqual(maxPages, query.Page);

                query.Page = 1;
                messages = service.GetMessages(query);
                Assert.AreEqual(int.Parse(configuration["MaxMessagesOnPage"]), messages.Count());
            }
        }

        
    }

    internal class Configuration : IConfiguration
    {
        public string this[string key]
        {
            get
            {
                switch (key)
                {
                    case "MaxMessagesOnPage":
                        return "3";
                    case "MaxMessageCountPerUser":
                        return "4";
                    case "MaxMessageCount":
                        return "10";
                    default:
                        return null;
                }
            }

            set => throw new NotImplementedException();
        }

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            throw new NotImplementedException();
        }

        public Microsoft.Extensions.Primitives.IChangeToken GetReloadToken()
        {
            throw new NotImplementedException();
        }

        public IConfigurationSection GetSection(string key)
        {
            throw new NotImplementedException();
        }
    }
}
