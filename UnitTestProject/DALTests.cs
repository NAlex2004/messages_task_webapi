using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages.DAL;
using Messages.DAL.Items;
using System;
using System.Linq;

namespace UnitTestProject
{
    [TestClass]
    public class DALTests
    {
        private IRepository<UserItem> _users;
        private IRepository<MessageItem> _messages;
        private int maxMessages = 10;
        private int maxMessagesPerUser = 5;

        private void InitRepositories()
        {
            _users = new UserInMemoryRepository();
            _messages = new MessageInMemoryRepository(maxMessages, maxMessagesPerUser);
        }

        private void FillUsersRepository()
        {
            for(int i = 0; i < 10; i++)
            {
                UserItem user = new UserItem()
                {
                    UserName = $"User #{i}"
                };
                _users.Add(user);
            }
        }

        private void FillMessagesForUser(UserItem user)
        {
            for(int i = 0; i < 15; i++)
            {
                MessageItem message = new MessageItem()
                {
                    MessageText = $"User {user.UserName} posted message #{i}",
                    UserGuid = user.Guid,
                    MessageDate = new DateTime(2000, 1, i+1, 0, i, 0, DateTimeKind.Utc)
                };

                _messages.Add(message);
            }
        }

        [TestMethod]
        public void AddedUser_HasGuid()
        {
            InitRepositories();

            UserItem user = new UserItem()
            {
                UserName = "Ass"
            };

            UserItem addedUser = _users.Add(user);

            Assert.AreNotEqual(Guid.Empty, addedUser.Guid);
        }

        [TestMethod]
        public void AddUSer_EmptyUserName_Throws()
        {
            InitRepositories();

            UserItem user = new UserItem();

            Assert.ThrowsException<ArgumentException>(() => _users.Add(user));
        }

        [TestMethod]
        public void AddMessage_EmptyUserGuid_or_EmptyMessageText_Throws()
        {
            InitRepositories();

            MessageItem message = new MessageItem()
            {
                MessageText = "text"
            };
            MessageItem message2 = new MessageItem()
            {
                UserGuid = Guid.NewGuid()
            };

            Assert.ThrowsException<ArgumentException>(() => _messages.Add(message));
            Assert.ThrowsException<ArgumentException>(() => _messages.Add(message2));

            message2.MessageText = "message";
            var addedMessage = _messages.Add(message2);

            Assert.IsNotNull(addedMessage.MessageDate);
        }

        [TestMethod]
        public void Get_WithExpression()
        {
            InitRepositories();
            FillUsersRepository();

            var users = _users.Get(u => int.Parse(u.UserName.Substring(u.UserName.IndexOf('#')).Substring(1)) > 5);
            Assert.AreEqual(4, users.Count());
        }

        [TestMethod]
        public void Remove_RemoveMany()
        {
            InitRepositories();
            FillUsersRepository();

            var user = _users.Get().FirstOrDefault();
            int count = _users.Get().Count();
            bool removed = _users.Remove(user);

            Assert.IsTrue(removed);
            Assert.AreEqual(count - 1, _users.Get().Count());

            int toBeRemovedCount = _users.Get(u => int.Parse(u.UserName.Substring(u.UserName.IndexOf('#')).Substring(1)) < 5).Count();

            _users.Remove(u => int.Parse(u.UserName.Substring(u.UserName.IndexOf('#')).Substring(1)) < 5);

            Assert.AreEqual(count - 1 - toBeRemovedCount, _users.Get().Count());
        }

        [TestMethod]
        public void UpdateItem()
        {
            InitRepositories();
            FillUsersRepository();

            var user = _users.Get().First();
            user.UserName = "Changed";

            var userChanged = _users.Get(u => u.UserName == "Changed").First();

            Assert.IsNotNull(userChanged);
            Assert.AreEqual("Changed", userChanged.UserName);
        }

        [TestMethod]
        public void MessagesLimitIsWorking()
        {
            InitRepositories();
            FillUsersRepository();

            UserItem user;
            UserItem user2;
            UserItem user3;

            var enumerator = _users.Get().AsEnumerable().GetEnumerator();
            enumerator.MoveNext();
            user = enumerator.Current;
            enumerator.MoveNext();
            user2 = enumerator.Current;
            enumerator.MoveNext();
            user3 = enumerator.Current;
            enumerator.Dispose();

            FillMessagesForUser(user);
            FillMessagesForUser(user2);

            Assert.AreEqual(maxMessagesPerUser, _messages.Get(m => m.UserGuid == user.Guid).Count());
            Assert.AreEqual(maxMessagesPerUser, _messages.Get(m => m.UserGuid == user2.Guid).Count());

            MessageItem msg = new MessageItem()
            {
                UserGuid = user3.Guid,
                MessageText = "Last"
            };

            _messages.Add(msg);

            Assert.AreEqual(maxMessages, _messages.Get().Count());
            FillMessagesForUser(user3);
            Assert.AreEqual(maxMessages, _messages.Get().Count());
        }
    }
}
