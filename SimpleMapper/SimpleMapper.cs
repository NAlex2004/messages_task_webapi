﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace NAlex.SimpleMapper
{
    public static class MapperExtensions
    {
        public static bool IsSystemType(this Type type) => type.Assembly == typeof(object).Assembly;        
    }

    /// <summary>
    /// Maps TSource object to TDestination.
    /// Properties names must be equal or have (in one of classes) BindedNameAttribute with property name of another class.
    /// Mapping should be created before Map by CreateMap method.
    /// Tries to map properties wich are classes recursively, mappings for them must exists.
    /// <example>
    /// SimpleMapper.CreateMap{SourceClass, DestinationClass}();
    /// DestinationClass destination = source.Map{sourceObject, SourceClass, DestinationClass}();
    /// </example>
    /// </summary>    
    public class SimpleMapper
    {        
        private List<Mappings> mappingsList = new List<Mappings>();
        internal IList<Mappings> MappingsList => mappingsList;

        /// <summary>
        /// Maps object of sourceType to object of destinationType.
        /// Properties names must be equal or have (in one of classes) BindedNameAttribute with property name of another class.
        /// Mapping should be created before Map by CreateMap method.
        /// </summary>
        public object Map(object source, Type sourceType, Type destinationType)
        {
            // бывает, блин..
            if (source == null)
                return null;            

            var mappings = mappingsList.Find(m => m.SourceType.Equals(sourceType) && m.DestinationType.Equals(destinationType));

            if (mappings == null)
            {
                throw new MapNotFoundException(string.Format("Mappings from type {0} to type {1} not found.", sourceType.Name, destinationType.Name));
            }

            var destination = Activator.CreateInstance(destinationType);

            foreach (var map in mappings.SourceDestinationPropertyMappings)
            {
                var sourceProperty = map.Key;                

                Type sourcePropertyType = sourceProperty.PropertyType;
                object sourcePropertyValue = sourceProperty.GetValue(source, null);

                if (!sourcePropertyType.IsSystemType())
                {
                    //var propertyMappings = mappingsList.Find(m => m.SourceType.Equals(sourcePropertyType) && m.DestinationType.Equals(map.Value.PropertyType));
                    var destPropertyValue = Map(sourcePropertyValue, sourcePropertyType, map.Value.PropertyType);
                    map.Value.SetValue(destination, destPropertyValue, null);
                }
                else
                {
                    map.Value.SetValue(destination, sourcePropertyValue, null);
                }                                
            }

            return destination;
        }

        /// <summary>
        /// Maps TSource object to TDestination.
        /// Properties names must be equal or have (in one of classes) BindedNameAttribute with property name of another class.
        /// Mapping should be created before Map by CreateMap method.
        /// </summary>
        public TDestination Map<TSource, TDestination>(TSource source) where TDestination: class, new()
        {
            if (source == null)
                return null;

            Type sourceType = source.GetType();
            Type destinationType = typeof(TDestination);

            return (TDestination)Map(source, sourceType, destinationType);            
        }

        public void CreateMap<TSource, TDestination>()
        {
            Type sourceType = typeof(TSource);
            Type destinationType = typeof(TDestination);
            Mappings mappings = new Mappings(sourceType, destinationType);

            if (mappingsList.Contains(mappings))
            {
                return;
            }

            PropertyInfo[] sourceProperties = sourceType.GetProperties();
            PropertyInfo[] destinationProperties = destinationType.GetProperties();

            foreach(var property in sourceProperties)
            {
                var attributes = property.GetCustomAttributes(true);
                
                if (attributes.FirstOrDefault(a => a.GetType() == typeof(NotMappedAttribute)) != null)
                {
                    continue;
                }

                var bindedAttribute = attributes.FirstOrDefault(a => a.GetType() == typeof(BindedNameAttribute));
                string bindedName = bindedAttribute != null ? ((BindedNameAttribute)bindedAttribute).BindedName : string.Empty;
                
                var destProperty = destinationProperties.FirstOrDefault(
                        p =>
                        {
                            if (p.Name.Equals(property.Name, StringComparison.OrdinalIgnoreCase)
                                || p.Name.Equals(bindedName, StringComparison.OrdinalIgnoreCase))
                            {
                                return true;
                            }

                            var destBindedAttribute = p.GetCustomAttributes(typeof(BindedNameAttribute), true).FirstOrDefault();

                            return (destBindedAttribute != null &&
                                ((BindedNameAttribute)destBindedAttribute).BindedName.Equals(property.Name));
                        }
                    );

                // Skip not mapped properties
                if (destProperty != null)
                {
                    mappings.SourceDestinationPropertyMappings.Add(property, destProperty);
                }
            }

            mappingsList.Add(mappings);
        }
    }
}
