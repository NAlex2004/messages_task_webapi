﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace NAlex.SimpleMapper.Expressions
{
    /// <summary>
    /// Usage:
    /// Create mappings with Mapper or SimpleMapper instance.
    /// Create expression.
    /// Call Convert method.
    /// Example:
    /// Mapper.CreateMap{ComplexDestClass, ComplexSourceClass}();
    /// Mapper.CreateMap{UserDest, UserSource}();
    /// Expression{Func{ComplexDestClass, int}} expr = d => d.User.Id;
    /// var mappedExpr = expr.Convert{ComplexDestClass, ComplexSourceClass, int}();
    /// var result = source.Select(mappedExpr.Compile());
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Converts expression of <cref>Func{TSource, TResult}</cref> to Func{TDestination, TResult} with TSource to TDestination conversion
        /// For type to type conversion Mapper class is used. No mappings - no conversion))
        /// </summary>
        public static Expression<Func<TDestination, TResult>> Convert<TSource, TDestination, TResult>(this Expression<Func<TSource, TResult>> sourceExpression)
        {
            return sourceExpression.Convert<TSource, TDestination, TResult>(Mapper.Instance);
        }

        /// <summary>
        /// Converts expression of <cref>Func{TSource, TResult}</cref> to Func{TDestination, TResult} with TSource to TDestination conversion
        /// For type to type conversion SimpleMapper instance is used. Can be null or empty. No mappings - no conversion))
        /// </summary>
        public static Expression<Func<TDestination, TResult>> Convert<TSource, TDestination, TResult>(this Expression<Func<TSource, TResult>> sourceExpression, SimpleMapper mapper)
        {
            Dictionary<Type, Type> paramsMappings = new Dictionary<Type, Type>();
            foreach (var parameter in sourceExpression.Parameters)
            {
                paramsMappings.Add(parameter.Type, parameter.Type.Equals(typeof(TSource))
                    ? typeof(TDestination)
                    : parameter.Type);
            }

            Dictionary<Type, Type> nestedTypeMappings = new Dictionary<Type, Type>();
            foreach(var map in mapper.MappingsList)
            {
                if (!nestedTypeMappings.ContainsKey(map.SourceType))
                {
                    nestedTypeMappings.Add(map.SourceType, map.DestinationType);
                }                
            }

            return ConvertWithNested<Func<TSource, TResult>, Func<TDestination, TResult>>(sourceExpression, paramsMappings, nestedTypeMappings);
        }

        /// <summary>
        /// Converts expression of TSourceDelegate to expression of TDestinationDelegate
        /// using Type-Type mapping dictionaries for parameters and any nested types.
        /// Ex: Expression{Func{Source, bool}} to Expression{Func{Destination, bool}}
        /// where Source and Destination have same members. 
        /// Nested types in Source and Destination have same members too.
        /// </summary>        
        /// <param name="parametersMappings">Type to Type mappings for parameters (source-destination)</param>
        /// <param name="nestedTypesMappings">Type to Type mappings for source and destination nested types</param>
        /// <returns></returns>
        public static Expression<TDestinationDelegate> ConvertWithNested<TSourceDelegate, TDestinationDelegate>
            (this Expression<TSourceDelegate> sourceExpression, Dictionary<Type, Type> parametersMappings, Dictionary<Type, Type> nestedTypesMappings)
        {
            // Key - source type, value - destination ParameterExpression
            Dictionary<Type, ParameterExpression> paramsMapping = new Dictionary<Type, ParameterExpression>();

            if (parametersMappings != null)
            {
                foreach (var parameter in sourceExpression.Parameters)
                {
                    Type paramDestType;
                    if (parametersMappings.TryGetValue(parameter.Type, out paramDestType))
                    {
                        paramsMapping.Add(parameter.Type, Expression.Parameter(paramDestType, parameter.Name));
                    }
                }
            }            

            DTOReplaceVisitor visitor = new DTOReplaceVisitor(paramsMapping, nestedTypesMappings);
            var body = visitor.Visit(sourceExpression.Body);

            var result = Expression.Lambda<TDestinationDelegate>(body, paramsMapping.Values);

            return result;
        }

    }
}
