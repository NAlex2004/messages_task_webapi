﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NAlex.SimpleMapper
{
    public class BindedNameAttribute: Attribute
    {
        private readonly string _bindedName;

        public BindedNameAttribute(string bindedName)
        {
            _bindedName = bindedName;
        }

        public string BindedName { get { return _bindedName; } }
    }
}
