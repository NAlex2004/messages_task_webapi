﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NAlex.SimpleMapper
{
    public class MapNotFoundException : Exception
    {
        public MapNotFoundException()
        {
        }

        public MapNotFoundException(string message) : base(message)
        {
        }
    }
}
