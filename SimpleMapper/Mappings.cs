﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace NAlex.SimpleMapper
{
    // after tests make internal
    internal class Mappings: IEquatable<Mappings>
    {
        public Type SourceType { get; protected set; }
        public Type DestinationType { get; protected set; }

        public Dictionary<PropertyInfo, PropertyInfo> SourceDestinationPropertyMappings { get; protected set; }

        public Mappings(Type sourceType, Type destinationType)
        {
            if (sourceType == null && destinationType == null)
            {
                throw new ArgumentNullException();
            }

            if (sourceType.Equals(destinationType))
            {
                throw new ArgumentException("sourceType and destinationType cannot be same types");
            }
            
            SourceType = sourceType;
            DestinationType = destinationType;
            SourceDestinationPropertyMappings = new Dictionary<PropertyInfo, PropertyInfo>();
        }

        public bool Equals(Mappings other)
        {
            if (other == null)
            {
                return false;
            }

            return SourceType.Equals(other.SourceType) && DestinationType.Equals(other.DestinationType);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Mappings);
        }

        public override int GetHashCode()
        {
            return SourceType.GetHashCode() ^ DestinationType.GetHashCode();
        }
    }
}
